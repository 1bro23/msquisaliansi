let list=[],baseList=[]
let input=document.querySelector("input")
fetch("./quisList.json")
.then(e=>e.json())
.then(data=>{
    baseList=data
    list=baseList
    updateTable()
})
function updateTable(){
    let tbody = document.querySelector("table tbody")
    tbody.innerHTML=""
    list.forEach(e=>{
        let tr=document.createElement("tr")
        tr.innerHTML=`<td>${e.question}</td>
        <td>${e.answear?"Benar":"Salah"}</td>`
        tbody.appendChild(tr)    
    })
}
function filter(e){
    list=baseList
    let filters = e.value.toLowerCase().split(' ')

    filters.forEach(e=>{
        list=list.filter(d=>d.question.toLowerCase().includes(e))
    })
    list.sort((a,b)=>{
        let rangeA=0,rangeB=0
        filters.forEach(e=>{
            rangeA+=a.question.toLowerCase().indexOf(e)
            rangeB+=b.question.toLowerCase().indexOf(e)
        })
        return rangeA-rangeB
    })
    updateTable()
}
window.onload=e=>input.focus()
window.onclick=e=>input.focus()